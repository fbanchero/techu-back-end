var requestJSON = require('request-json');
var config = require('../configuration.js');

// GET users
// exports.all = function(req, res, next) {
//   console.log("asdadasd");
//   next();
// };

// GET transactions
exports.getTransactions = (req, res) => {
  let userid = req.headers.userid;
  let idAccount = req.params.id;
  let typeFilter = '';
  let startDateFilter = '';
  let endDateFilter = '';
  let existFilters = false;
  if (req.query.type != undefined) {
    typeFilter = ',{"type":"' + req.query.type + '"}';
    existFilters = true;
  }
  if (req.query.startDate != undefined) {
    startDateFilter = ',{"date":{"$gt":"' + req.query.startDate + 'T00:00:00.000Z"}}';
    existFilters = true;
  }
  if (req.query.endDate != undefined) {
    endDateFilter = ',{"date":{"$lt":"' + req.query.endDate + 'T00:00:00.000Z"}}';
    existFilters = true;
  }
  var httpClient = requestJSON.createClient(config.urlMlab);
  var queryString;
  if (existFilters) {
    queryString = '&q={$and:[{$or:[{"accountsIdReceiver":' + idAccount + '},{"accountsIdSender":' + idAccount + '}]}'
    + typeFilter
    + startDateFilter
    + endDateFilter
    + ']}';
  } else {
    queryString = '&q={$or:[{"accountsIdReceiver":' + idAccount + '},{"accountsIdSender":' + idAccount + '}]}';
  }
  console.log(queryString);
  var queryFilters = '&f={"_id":0}';
  var sort = '&s={"date":-1}';
  console.log(config.urlMlab + 'transactions' + config.apiKeyMlab + queryString + queryFilters + sort);
  httpClient.get(config.urlMlab + 'transactions' + config.apiKeyMlab + queryString + queryFilters + sort,
    function(err, respuestaMLab, body) {
      var response = {};
      if(err) {
          response = {
            "msg" : "Error obteniendo transacciones de cuenta."
          }
          res.status(500);
      } else {
        if(body.length > 0) {
          var response = parseTransactionsList(idAccount, body);
          res.status(200);
        } else {
          response = {
            "msg" : "Sin transacciones."
          }
          res.status(404);
        }
      }
      res.send(response);
    });

};

// GET transaction
exports.getTransaction = (req, res) => {
  var userid = req.headers.userid;
  let idAccount = req.params.id;
  let idTransaction = req.params.idTransaction;
  var httpClient = requestJSON.createClient(config.urlMlab);
  var queryString = '&q={$or:[{"accountsIdReceiver":' + idAccount + ',id:' + idTransaction + '},{"accountsIdSender":' + idAccount + ',id:' + idTransaction + '}]}';
  var queryFieldString = '&f={"_id":0}';
  console.log(config.urlMlab + 'transactions' + config.apiKeyMlab + queryString + queryFieldString);
  httpClient.get(config.urlMlab + 'transactions' + config.apiKeyMlab + queryString + queryFieldString,
    function(err, respuestaMLab, body) {
      var response = {};
      if(err) {
          response = {
            "msg" : "Error obteniendo transaccion."
          }
          res.status(500);
          res.send(response);
      } else {
        if(body.length > 0) {
          response = parseTransaction(idAccount, body[0]);
          res.status(200);
          res.send(response);
        } else {
          response = {
            "msg" : "Transaccion no encontrada."
          }
          res.status(404);
          res.send(response);
        }
      }
    });

}

// POST transaction
exports.postTransaction = (req, res) => {
  console.log(req.body);
  var userid = req.headers.userid;
  let idAccount = req.params.id;
  var clienteMlab = requestJSON.createClient(config.urlMlab);
  let newTransaction = {};
  newTransaction.id = newId();
  newTransaction.date = stringToDate(req.body.date);
  newTransaction.description = req.body.description;
  newTransaction.type = req.body.type;
  var amountSender = parseFloat(parseFloat(req.body.amount).toFixed(2));
  var amountReceiver = parseFloat((parseFloat(req.body.amount)*req.body.exchangeRate).toFixed(2));
  newTransaction.amountSender = amountSender;
  newTransaction.amountReceiver = amountReceiver;
  newTransaction.accountsIdReceiver = parseInt(req.body.accountsIdReceiver, 10);
  newTransaction.accountsIdSender = parseInt(idAccount, 10);
  let setAccount = '{"$set" : ' + JSON.stringify({"balance":parseFloat((req.body.balanceSource - amountSender).toFixed(2)) }) + '}';
  let jsonSetAccount = JSON.parse(setAccount);
  clienteMlab.put(config.urlMlab + "accounts/" + req.body._idSource + config.apiKeyMlab, jsonSetAccount,
    function(error, respuestaMLab, body){
      console.log("Saldo origen actualizado");
    });
  setAccount = '{"$set" : ' + JSON.stringify({"balance":parseFloat((req.body.balanceTarget + amountReceiver).toFixed(2))}) + '}';
  jsonSetAccount = JSON.parse(setAccount);
  clienteMlab.put(config.urlMlab + "accounts/" + req.body._idTarget + config.apiKeyMlab, jsonSetAccount,
    function(error, respuestaMLab, body){
      console.log("Saldo destino actualizado");
    });
  clienteMlab.post(config.urlMlab + "transactions" + config.apiKeyMlab, newTransaction,
    function(error, respuestaMLab, body){
      let result = parseTransaction(idAccount, body);
      res.status(200);
      res.send(result);
    });
}

// DELETE transaction
exports.deleteTransaction = (req, res) => {
  var userid = req.headers.userid;
  let idAccount = req.params.id;
  let idTransaction = req.params.idTransaction;
  var clienteMlab = requestJSON.createClient(config.urlMlab);
  var httpClient = requestJSON.createClient(config.urlMlab);
  var queryString = '&q={$or:[{"accountsIdReceiver":' + idAccount + ',id:' + idTransaction + '},{"accountsIdSender":' + idAccount + ',id:' + idTransaction + '}]}';
  var queryFieldString = '&f={"_id":1}';
  httpClient.get(config.urlMlab + 'transactions' + config.apiKeyMlab + queryString + queryFieldString,
    function(err, respuestaMLab, body) {
      var response = {};
      if(err) {
          response = {
            "msg" : "Error obteniendo transaccion."
          }
          res.status(500);
          res.send(response);
      } else {
        if(body.length > 0) {
          console.log(body[0]);
          clienteMlab.delete(config.urlMlab + "transactions/" + body[0]._id.$oid + config.apiKeyMlab,
            function(error, respuestaMLab, body){
              response = {
                "msg" : "Transaccion " + idTransaction + " borrada con éxito."
              };
              res.status(200);
              res.send(response);
            });
        } else {
          response = {
            "msg" : "Transaccion no encontrada."
          }
          res.status(404);
          res.send(response);
        }
      }
    });
}

exports.getAccountSource = (req, res, next) => {
  let idAccount = req.params.id;
  let amount = req.body.amount;
  var httpClient = requestJSON.createClient(config.urlMlab);
  var queryString = '&q={"id":' + idAccount + '}';
  httpClient.get('accounts' + config.apiKeyMlab + queryString,
    function(err, respuestaMLab, body) {
      var response = {};
      if(err) {
          response = {
            "msg" : "Error obteniendo cuenta origen."
          }
          res.status(500);
          res.send(response);
      } else {
        if(body.length > 0) {
          response = body[0];
          req.body._idSource = response._id.$oid;
          req.body.balanceSource = response.balance;
          req.body.currencySource = response.currency;
          if (req.body.currencySource < amount) {
            response = {
              "msg" : "Saldo Insuficiente."
            }
            res.status(400);
            res.send(response);
          } else {
            next();
          }
        } else {
          response = {
            "msg" : "Cuenta origen no encontrada."
          }
          res.status(404);
          res.send(response);
        }
      }
    });
}

exports.getAccountTarget = (req, res, next) => {
  let idAccount = req.body.accountsIdReceiver;
  let amount = req.body.amount;
  var httpClient = requestJSON.createClient(config.urlMlab);
  var queryString = '&q={"id":' + idAccount + '}';
  httpClient.get('accounts' + config.apiKeyMlab + queryString,
    function(err, respuestaMLab, body) {
      var response = {};
      if(err) {
          response = {
            "msg" : "Error obteniendo cuenta destino."
          }
          res.status(500);
          res.send(response);
      } else {
        if(body.length > 0) {
          response = body[0];
          req.body._idTarget = response._id.$oid;
          req.body.balanceTarget = response.balance;
          req.body.currencyTarget = response.currency;
          next();
        } else {
          response = {
            "msg" : "Cuenta destino no encontrada."
          }
          res.status(404);
          res.send(response);
        }
      }
    });
}

exports.getCurrencyConverter = (req, res, next) => {
  req.body.exchangeRate = 1;
  if (req.body.currencyTarget != req.body.currencySource) {
    var httpClient = requestJSON.createClient(config.urlCurrencyConverter);
    httpClient.get('?q=' + req.body.currencySource + '_' + req.body.currencyTarget,
      function(err, resp, body) {
        if (err) {
          response = {
            "msg" : "Error obteniendo tipo de cambio."
          }
          res.status(400);
          res.send(response);
        } else {
          let bodyResp = JSON.parse(resp.body);
          req.body.exchangeRate = bodyResp.results[req.body.currencySource + '_' + req.body.currencyTarget].val;
          next();
        }
      })
  } else {
    next();
  }
}

function newId() {
  let num = Math.floor(Math.random()*(999999999-100000000+1))+100000000;
  return num;
}

function parseTransactionsList(idAccount, body) {
  var transactions = [];
  for(var key in body) {
    transactions.push(parseTransaction(idAccount, body[key]));
  }
  return transactions;
}

function parseTransaction(idAccount, element) {
  var t = {};
  t.id = element.id;
  t.date = dateToString(element.date);
  t.description = element.description;
  t.type = element.type;
  switch (element.type) {
    case "internet_transfer":
      t.typeName = "Transferencia por internet";
      break;
    case "salary_payment":
      t.typeName = "Pago de sueldo";
      break;
    case "rent_payment":
      t.typeName = "Alquiler";
      break;
    default:
      t.typeName = "Otros";
  }
  if (element.accountsIdSender == idAccount) { // Es un debito
    t.amount = -1*element.amountSender;
    t.otherAccount = element.accountsIdReceiver;
  } else { // Es un credito
    t.amount = element.amountReceiver;
    t.otherAccount = element.accountsIdSender;
  }
  return t;
}

// parse a date in yyyy-mm-dd format
function stringToDate(input) {
  console.log(input);
  var parts = input.split('T');
  parts = parts[0].split('-');
  console.log(parts[0] + '-' + parts[1] + '-' + parts[2]);
  // new Date(year, month [, day [, hours[, minutes[, seconds[, ms]]]]])
  return new Date(parts[0], parts[1]-1, parts[2]); // Note: months are 0-based
}

function dateToString(input) {
  var parts = input.split('T');
  parts = parts[0].split('-');
  return parts[2] + '/' + parts[1] + '/' +  parts[0];
}
