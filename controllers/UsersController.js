var express = require('express');
var bodyParser = require('body-parser');
var requestJSON = require('request-json');
var app = express();
app.use(bodyParser.json());
var config = require('../configuration.js');


exports.getUsers = (req, res) => {
  var httpClient = requestJSON.createClient(config.urlMlab);
  var queryString = '&f={"_id":0}';
  httpClient.get('users' + config.apiKeyMlab + queryString,
    function(err, respuestaMLab, body) {
      var response = {};
      if(err) {
          response = {
            "msg" : "Error obteniendo usuario."
          }
          res.status(500);
      } else {
        if(body.length > 0) {
          response = body;
        } else {
          response = {
            "msg" : "Usuario no encontrado."
          }
          res.status(404);
        }
      }
      res.send(response);
    });
};

// GET userId
exports.getUser = (req, res) => {
  let idUser = req.params.id;
  var httpClient = requestJSON.createClient(config.urlMlab);
  var queryString = '&q={"id":' + idUser + '}';
  var queryFieldString = '&f={"_id":0}';
  console.log(config.urlMlab + 'users' + config.apiKeyMlab + queryString + queryFieldString);
  httpClient.get('users' + config.apiKeyMlab + queryString + queryFieldString,
    function(err, respuestaMLab, body) {
      var response = {};
      if(err) {
          response = {
            "msg" : "Error obteniendo usuario."
          }
          res.status(500);
      } else {
        if(body.length > 0) {
          response = body[0];
        } else {
          response = {
            "msg" : "Usuario no encontrado."
          }
          res.status(404);
        }
      }
      res.send(response);
    });
};

// POST users
exports.postUser = (req, res) => {
  var clienteMlab = requestJSON.createClient(config.urlMlab);
  clienteMlab.get('users' + config.apiKeyMlab,
    function(error, respuestaMLab, body){
      newID = body.length + 1;
      console.log("newID:" + newID);
      let newUser = {};
      newUser.id = newID;
      newUser.firstName = req.body.firstName;
      newUser.lastName = req.body.lastName;
      newUser.document = req.body.document;
      newUser.email = req.body.email;
      newUser.userName = req.body.username;
      newUser.password = req.body.password;
      console.log(newUser);
      clienteMlab.post(config.urlMlab + "users" + config.apiKeyMlab, newUser,
        function(error, respuestaMLab, body){
          res.send(body);
        });
    });
};

// PUT users
exports.putUser = (req, res) => {
  var clienteMlab = requestJSON.createClient(config.urlMlab);
  let idUser = req.params.id;
  var httpClient = requestJSON.createClient(config.urlMlab);
  var queryString = '&q={"id":' + idUser + '}';
  var queryFieldString = '&f={"_id":1}';
  httpClient.get('users' + config.apiKeyMlab + queryString + queryFieldString,
    function(err, respuestaMLab, body) {
      var response = {};
      if(err) {
          response = {
            "msg" : "Error obteniendo usuario."
          }
          res.status(500);
          res.send(response);
      } else {
        if(body.length > 0) {
          let setUser = '{"$set" : ' + JSON.stringify(req.body) + '}';
          let jsonSetUser = JSON.parse(setUser);
          clienteMlab.put(config.urlMlab + "users/" + body[0]._id.$oid + config.apiKeyMlab, jsonSetUser,
            function(error, respuestaMLab, body){
              response = {
                "msg" : "Usuario " + idUser + " actualizado con exito."
              };
              res.status(200);
              res.send(response);
            });

        } else {
          response = {
            "msg" : "Usuario no encontrado."
          }
          res.status(404);
          res.send(response);
        }
      }
    });
};

// DELETE user
exports.deleteUser = (req, res) => {
  var clienteMlab = requestJSON.createClient(config.urlMlab);
  let idUser = req.params.id;
  var httpClient = requestJSON.createClient(config.urlMlab);
  var queryString = '&q={"id":' + idUser + '}';
  var queryFieldString = '&f={"_id":1}';
  console.log(config.urlMlab + 'users' + config.apiKeyMlab + queryString + queryFieldString);
  httpClient.get('users' + config.apiKeyMlab + queryString + queryFieldString,
    function(err, respuestaMLab, body) {
      var response = {};
      if(err) {
          response = {
            "msg" : "Error obteniendo usuario."
          }
          res.status(500);
          res.send(response);
      } else {
        if(body.length > 0) {
          clienteMlab.delete(config.urlMlab + "users/" + body[0]._id.$oid + config.apiKeyMlab,
            function(error, respuestaMLab, body){
              response = {
                "msg" : "Usuario " + idUser + " borrado con exito."
              };
              res.status(200);
              res.send(response);
            });

        } else {
          response = {
            "msg" : "Usuario no encontrado."
          }
          res.status(404);
          res.send(response);
        }
      }
    });
};
