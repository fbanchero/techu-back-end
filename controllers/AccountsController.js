var requestJSON = require('request-json');
var config = require('../configuration.js');

exports.getAccounts = (req, res) => {
  var httpClient = requestJSON.createClient(config.urlMlab);
  var userid = req.headers.userid;
  var queryString = '&q={"userId":' + userid + '}';
  var queryFilters = '&f={"_id":0}';
  console.log(config.urlMlab + 'accounts' + config.apiKeyMlab + queryString + queryFilters);
  httpClient.get(config.urlMlab + 'accounts' + config.apiKeyMlab + queryString + queryFilters,
    function(err, respuestaMLab, body) {
      var response = {};
      if(err) {
          response = {
            "msg" : "Error obteniendo cuentas de usuario."
          }
          res.status(500);
          res.send(response);
      } else {
        if(body.length > 0) {
          response = body;
          res.status(200);
          res.send(response);
        } else {
          response = {
            "msg" : "Usuario no encontrado."
          }
          res.status(404);
          res.send(response);
        }
      }
    });
};

// GET accountId
exports.getAccount = (req, res) => {
  var userid = req.headers.userid;
  let idAccount = req.params.id;
  var httpClient = requestJSON.createClient(config.urlMlab);
  var queryString = '&q={"id":' + idAccount + ',userId:' + userid + '}';
  var queryFieldString = '&f={"_id":0}';
  console.log(config.urlMlab + 'accounts' + config.apiKeyMlab + queryString + queryFieldString);
  httpClient.get('accounts' + config.apiKeyMlab + queryString + queryFieldString,
    function(err, respuestaMLab, body) {
      var response = {};
      if(err) {
          response = {
            "msg" : "Error obteniendo cuenta."
          }
          res.status(500);
          res.send(response);
      } else {
        if(body.length > 0) {
          response = body[0];
          res.status(200);
          res.send(response);
        } else {
          response = {
            "msg" : "Cuenta no encontrada."
          }
          res.status(404);
          res.send(response);
        }
      }
    });
};

// POST account
exports.postAccount = (req, res) => {
  var userid = req.headers.userid;
  var clienteMlab = requestJSON.createClient(config.urlMlab);
    let newAccount = {};
    newAccount.id = newAccountNumber();
    newAccount.userId = userid;
    newAccount.balance = req.body.balance;
    newAccount.description = req.body.description;
    newAccount.currency = req.body.currency;
    clienteMlab.post(config.urlMlab + "accounts" + config.apiKeyMlab, newAccount,
      function(error, respuestaMLab, body){
        res.send(body);
      });
};

// DELETE account
exports.deleteAccount = (req, res) => {
  var userid = req.headers.userid;
  var clienteMlab = requestJSON.createClient(config.urlMlab);
  let id = req.params.id;
  var httpClient = requestJSON.createClient(config.urlMlab);
  var queryString = '&q={"id":' + id + '}';
  var queryString = '&q={"id":' + id + ',userId:' + userid + '}';
  var queryFieldString = '&f={"_id":1}';
  console.log(config.urlMlab + 'accounts' + config.apiKeyMlab + queryString + queryFieldString);
  httpClient.get('accounts' + config.apiKeyMlab + queryString + queryFieldString,
    function(err, respuestaMLab, body) {
      var response = {};
      if(err) {
          response = {
            "msg" : "Error obteniendo cuenta."
          }
          res.status(500);
          res.send(response);
      } else {
        if(body.length > 0) {
          clienteMlab.delete(config.urlMlab + "accounts/" + body[0]._id.$oid + config.apiKeyMlab,
            function(error, respuestaMLab, body){
              response = {
                "msg" : "Cuenta " + id + " borrada con éxito."
              };
              res.status(200);
              res.send(response);
            });

        } else {
          response = {
            "msg" : "Cuenta no encontrada."
          }
          res.status(404);
          res.send(response);
        }
      }
    });
};

// PUT users
exports.updateAccount = (req, res) => {
  var userid = req.headers.userid;
  let id = req.params.id;
  var clienteMlab = requestJSON.createClient(config.urlMlab);
  var httpClient = requestJSON.createClient(config.urlMlab);
  var queryString = '&q={"id":' + id + ',userId:' + userid + '}';
  var queryFieldString = '&f={"_id":1}';
  httpClient.get('accounts' + config.apiKeyMlab + queryString + queryFieldString,
    function(err, respuestaMLab, body) {
      var response = {};
      if(err) {
          response = {
            "msg" : "Error obteniendo cuenta."
          }
          res.status(500);
          res.send(response);
      } else {
        if(body.length > 0) {
          let setAccount = '{"$set" : ' + JSON.stringify(req.body) + '}';
          let jsonSetAccount = JSON.parse(setAccount);
          clienteMlab.put(config.urlMlab + "accounts/" + body[0]._id.$oid + config.apiKeyMlab, jsonSetAccount,
            function(error, respuestaMLab, body){
              response = {
                "msg" : "Cuenta " + id + " actualizada con éxito."
              };
              res.status(200);
              res.send(response);
            });

        } else {
          response = {
            "msg" : "Cuenta no encontrada."
          }
          res.status(404);
          res.send(response);
        }
      }
    });
};


function newAccountNumber() {
  let num = Math.floor(Math.random()*(999999999-100000000+1))+100000000;
  return num;
}
