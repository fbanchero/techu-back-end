var express = require('express');
var requestJSON = require('request-json');
var app = express();
var config = require('../configuration.js');

var jwt = require('jsonwebtoken');

//app.use(bodyParser.urlencoded({extended: false}))
//app.use(bodyParser.json({limit:'10mb'}))


// POST login
exports.login = (req, res) => {
  var username = req.body.username
  var password = req.body.password

  var httpClient = requestJSON.createClient(config.urlMlab);
  var queryString = '&q={"userName":"' + username + '", "password":"' + password + '"}';
  var queryFilters = '&f={"_id":0,"password":0}';
  httpClient.get('users' + config.apiKeyMlab + queryString + queryFilters,
    function(err, respuestaMLab, body) {
      var response = {};
      if(err) {
          response = {
            "msg" : "Error obteniendo usuario."
          }
          res.status(500);
          res.send(response);
      } else {
        if(body.length > 0) {
          var tokenData = {
            username: username
          }
          var token = jwt.sign(tokenData, 'Es un secreto', {
             expiresIn: 60 * 60 * 24 // expires in 24 hours
          })
          let newToken = {};
          newToken.username = username;
          newToken.userid = body[0].id;
          newToken._id = token;
          response = {
            "userData": body[0],
            "token":token
          };
          console.log(response);
          httpClient.post(config.urlMlab + "tokens" + config.apiKeyMlab, newToken,
            function(error, respuestaMLab, body){
              console.log("Login Exitoso");
              console.log(body);
              res.status(200);
              res.send(response);
            });

        } else {
          response = {
            "msg" : "Usuario no encontrado."
          }
          res.status(404);
          res.send(response);
        }
      }
    });
};

// POST logout
exports.logout = (req, res) => {
  let token = req.headers.Authorization;
  var user = util.tokenValidate(token);
  if (user == undefined) {
    response = {
      "msg" : "Token de seguridad no válido."
    };
    res.status(400);
    res.send(response);
  } else {
    var httpClient = requestJSON.createClient(config.urlMlab);
    var queryString = '&q={"jwt":"' + token + '"}';
    httpClient.delete(config.urlMlab + "tokens/" + token + config.apiKeyMlab,
      function(error, respuestaMLab, body){
        response = {
          "msg" : "El usuario " + body.username + " ha salido correctamente."
        };
        res.status(200);
        res.send(response);
      });
  }
};

function tokenValidate(token) {
  if(!token){
      return null;
  }
  token = token.replace('Bearer ', '')
  var username = jwt.verify(token, 'Es un secreto', function(err, user) {
    if (err) {
      return null;
    } else {
      return user.username;
    }
  })
  return username;
};
