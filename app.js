var express = require('express');
const URI = "/api-uruguay/v1";
var app = express();
var config = require('./configuration.js');
var bodyParser = require('body-parser');
var cors = require('cors')

// app.use(bodyParser.json(),function(req, res, next) {
//   res.header("Access-Control-Allow-Origin", "*");
//   res.header("Access-Control-Allow-Methods","GET,PUT,POST,DELETE,PATCH")
//   res.header("Access-Control-Allow-Headers", "authorization");
//   next();
// });
app.use(bodyParser.json());
app.use(cors({
  "origin": "*",
  "methods": "GET,HEAD,PUT,PATCH,POST,DELETE",
  "preflightContinue": false,
  "optionsSuccessStatus": 204
}));

const usersRoute = require("./routes/UsersRoute");
const accountsRoute = require("./routes/AccountsRoute");
const authenticationRoute = require("./routes/AuthenticationRoute");

app.use(URI + "/users", usersRoute);
app.use(URI + "/accounts", accountsRoute);
app.use(URI + "/authentication", authenticationRoute);

app.listen(config.port);
console.log("Esta app esta escuchando en el puerto " + config.port);
