# Imagen docker base
FROM node

RUN mkdir -p /docker-back

# Definimos directorio de trabajo de Docker
WORKDIR /docker-back

# Copia archivos del proyecto al directorio
ADD . /docker-back

# Para ejecutar un comando antes de lanzar la aplicacion, por ejemplo instalar las dependencias del proyecto
# RUN npm install

# Puerto donde exponemos el contenedor
#EXPOSE 3000

# Comandos para lanzar la aplicacion
CMD ["npm", "run-script", "start"]
