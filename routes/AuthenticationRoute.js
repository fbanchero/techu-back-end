const router = require("express").Router();
const controller = require("./../controllers/AuthenticationController");

/*
 * /api-uruguay/v1/authentication/login     POST
 * /api-uruguay/v1/authentication/logout    POST
 */

router.route("/login")
  .post(controller.login);

router.route("/logout")
  .post(controller.logout);

module.exports = router;
