const router = require("express").Router();
const accountsController = require("./../controllers/AccountsController");
const transactionsController = require("./../controllers/TransactionsController");

// const util= require('./../utils/Util');
/*
 * /api-uruguay/v1/accounts/     GET    - READ ALL
 * /api-uruguay/v1/accounts/     POST   - CREATE
 * /api-uruguay/v1/accounts/:id  GET    - READ ONE
 * /api-uruguay/v1/accounts/:id  PUT    - UPDATE
 * /api-uruguay/v1/accounts/:id  DELETE - DELETE
 */
 const tokenValidate= require('./../utils/Util').tokenValidate;
 const isLoggedIn= require('./../utils/Util').isLoggedIn;
 router.use(tokenValidate);
 router.use(isLoggedIn);

router.route("/")
  .get(accountsController.getAccounts)
  .post(accountsController.postAccount);

router.route("/:id")
  .get(accountsController.getAccount)
  .delete(accountsController.deleteAccount)
  .put(accountsController.updateAccount);

router.route("/:id/transactions")
  .get(transactionsController.getTransactions)
  .post(transactionsController.getAccountSource,
    transactionsController.getAccountTarget,
    transactionsController.getCurrencyConverter, 
    transactionsController.postTransaction);

router.route("/:id/transactions/:idTransaction")
  .get(transactionsController.getTransaction)
  .delete(transactionsController.deleteTransaction);


module.exports = router;
