const router = require("express").Router();
const controller = require("./../controllers/UsersController");

/*
 * /api-uruguay/v1/users/     GET    - READ ALL
 * /api-uruguay/v1/users/     POST   - CREATE
 * /api-uruguay/v1/users/:id  GET    - READ ONE
 * /api-uruguay/v1/users/:id  PUT    - UPDATE
 * /api-uruguay/v1/users/:id  DELETE - DELETE
 */

router.route("/")
  .get(controller.getUsers)
  .post(controller.postUser);

router.route("/:id")
  .get(controller.getUser)
  .post(controller.postUser)
  .put(controller.putUser)
  .delete(controller.deleteUser);

module.exports = router;
