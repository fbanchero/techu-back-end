var jwt = require('jsonwebtoken');
var config = require('../configuration.js');
var requestJSON = require('request-json');
var response;

function tokenValidate(req, res, next) {
  let token = req.headers.authorization;
  if (token == undefined) {
    response = {
      "msg" : "Usuario no autenticado."
    };
    res.status(400);
    res.send(response);
  }
  if(!token){
    response = {
      "msg" : "Error validando token de seguridad."
    };
    res.status(400);
    res.send(response);
  }
  token = token.replace('Bearer ', '')
  var username = jwt.verify(token, 'Es un secreto', function(err, user) {
    if (err) {
      response = {
        "msg" : "Token de seguridad no válido."
      };
      res.status(400);
      res.send(response);
    } else {
      return next();

    }
  })

};

function isLoggedIn(req, res, next) {
  let token = req.headers.authorization;
  var httpClient = requestJSON.createClient(config.urlMlab);
  httpClient.get(config.urlMlab + "tokens/" + token + config.apiKeyMlab,
    function(error, respuestaMLab, body){
      if (error) {
        response = {
          "msg" : "Error validando usuario."
        }
        res.status(500);
        res.send(response);
      } else {
        if(body != undefined) {
          req.headers.userid = body.userid;
          return next();
        } else {
          response = {
            "msg" : "Usuario inválido."
          }
          res.status(404);
          res.send(response);
        }
      }
    });
}

/*module.exports = tokenValidate.tokenValidate = tokenValidate;
module.exports = isLoggedIn;*/
module.exports = {
  tokenValidate: tokenValidate,
  isLoggedIn: isLoggedIn
}
